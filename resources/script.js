"use strict";

function buildTableFromJson(JSON_DATA)
{
	// Get keys
	var JKEYS = Object.keys(JSON_DATA[0]);

	// Build base table
	var TABLE = document.createElement('table');
	var FIRST_LINE = document.createElement('tr');

	// Build column titles
	for (let KEY of JKEYS)
	{
		let COL_TITLE = document.createElement('th');
		let TXT = document.createTextNode(KEY);
		COL_TITLE.appendChild(TXT);
		FIRST_LINE.appendChild(COL_TITLE);
	}
	TABLE.appendChild(FIRST_LINE);

	// Build lines
	function buildLine(JSON_LINE)
	{
		let LINE = document.createElement('tr');
		for (let FIELD in JSON_LINE)
		{
			let ELEM = document.createElement('td');
			ELEM.setAttribute('contenteditable', true);
			let TXT = document.createTextNode(JSON_LINE[FIELD]);
			ELEM.appendChild(TXT);
			LINE.appendChild(ELEM);
		}
		return LINE
	}

	for (let LINE of JSON_DATA)
	{
		let ENDING = buildLine(LINE);
		TABLE.appendChild(ENDING);
	}

    // Put the table in the document
    document.getElementById('putTableHere').innerHTML = TABLE.innerHTML;
}
