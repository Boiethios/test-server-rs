# Get started

## Install the rust environment

Install `rustup`:

```
$ curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain nightly
```

Normally, `rustc` and `cargo` are automaticaly installed. Verify:

```
$ rustc --version
```

If `rustc` is not found:

```
$ rustup update && rustup install rustc cargo
```

## Setup the database

```
$ cargo install diesel_cli --no-default-features --features sqlite
$ ./setup_database.sh install
```

To remove all the databases, just type:

```
$ ./setup_database.sh remove
```

## Launch!

Go in the project root and run:

```
$ cargo run
```

This may take a while the first time, the tool needs to download and build dependencies.
    
# Server

The server can get all the files from the `resources` directory.

It is launched on the port `8000` in debug mode: just go to http://localhost:8000/

# Use the JSON API

The API uses the CRUD model. You can test every operation with curl in your
terminal.

## Create

```
$ curl -H "Content-Type: application/json" -X POST -d '{"last_name": "<last name>", "first_name": "<first name>", "age": <age>, "email": "<mail>"}' http://localhost:8000/create
```

## Read

```
$ curl -X POST http://localhost:8000/read
```

## Update

```
$ curl -H "Content-Type: application/json" -X POST -d '{"id": "<id>", "updated": {"<field>": <new value>} }' http://localhost:8000/update
```

## Delete

```
$ curl -H "Content-Type: application/json" -X POST -d '"<id>"' http://localhost:8000/delete
```
