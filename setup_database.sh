#!/bin/bash

install() {

	diesel setup
	if [ $? -ne 0 ]; then
		echo ">>> Diesel could not setup."
		exit 1
	fi

	OUTPUT=`diesel migration generate database`
	if [ $? -ne 0 ]; then
		echo ">>> Diesel could not migrate database."
		exit 1
	fi
	for LINE in $OUTPUT; do
		if [[ $LINE == migrations/* ]]; then
			MIGR_FILE=`echo $LINE | cut -d ' ' -f2`
			FILE=`echo $MIGR_FILE | cut -d/ -f3`
			echo ">>> copying schemas/$FILE to $MIGR_FILE"
			cp -f schemas/$FILE $MIGR_FILE
		fi
	done

	diesel migration run
	if [ $? -ne 0 ]; then
		echo ">>> Diesel could run migration."
		exit 1
	fi

	echo ">>> Installation finished."
}

uninstall() {

	rm -r migrations db.sqlite
}

if [[ $1 == "install" ]]; then
	install
elif [[ $1 == "remove" ]]; then
	uninstall
else
	echo "usage:"
	echo "    install install a new database"
	echo "    remove  remove ALL the databases"
fi
