extern crate diesel;
extern crate dotenv;
extern crate uuid;

use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use self::dotenv::dotenv;
use std::env;

use self::uuid::Uuid;

pub type Id = String; //UUid

#[derive(Serialize, Deserialize, Queryable)]
#[derive(Insertable)]
#[table_name="persons"]
pub struct PersonEntry {
    pub id: Id,
    pub last_name: String,
    pub first_name: String,
    pub age: i32,
    pub email: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct PersonInfos {
    pub last_name: String,
    pub first_name: String,
    pub age: i32,
    pub email: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub enum PersonUpdateField {
    #[serde(rename="last_name")]    LastName(String),
    #[serde(rename="first_name")]   FirstName(String),
    #[serde(rename="age")]          Age(i32),
    #[serde(rename="email")]        Email(Option<String>),
}

#[derive(Serialize, Deserialize)]
pub struct PersonUpdate {
    pub id: Id,
    pub updated: PersonUpdateField,
}

impl PersonInfos {
    pub fn into_new_person_entry(self) -> PersonEntry {
        PersonEntry {
            id: Uuid::new_v4().hyphenated().to_string(),
            last_name: self.last_name,
            first_name: self.first_name,
            age: self.age,
            email: self.email,
        }
    }
}

infer_schema!("dotenv:DATABASE_URL");

pub fn establish_connection() -> SqliteConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}