extern crate diesel;

use db;
use db::persons::dsl::*;
use diesel::prelude::*;

use std::result::Result;
use std::vec::Vec;

pub struct ServerData {
    pub connection: diesel::sqlite::SqliteConnection,
}

impl Default for ServerData {
    fn default() -> Self {
        ServerData {
            connection: db::establish_connection(),
        }
    }
}

impl ServerData {
    pub fn new() -> Self {
        Default::default()
    }
    
    pub fn create(&mut self, person: db::PersonInfos)
        -> Result<usize, diesel::result::Error> {

        let person = person.into_new_person_entry();

        diesel::insert(&person).into(db::persons::table)
            .execute(&self.connection)
    }

    pub fn read(&mut self)
        -> Result<Vec<db::PersonEntry>, diesel::result::Error> {

        db::persons::table.load::<db::PersonEntry>(&self.connection)
    }

    pub fn update(&mut self, to_update: db::PersonUpdate)
        -> Result<usize, diesel::result::Error> {

        use db::PersonUpdateField::*;

        let update = diesel::update(persons.find(to_update.id));
        let ref c = self.connection;

        match to_update.updated {
            FirstName(val)  => update.set(first_name.eq(&val)).execute(c),
            LastName(val)   => update.set(last_name.eq(&val)).execute(c),
            Age(val)        => update.set(age.eq(&val)).execute(c),
            Email(val)      => update.set(email.eq(&val)).execute(c),
        }
    }

    pub fn delete(&mut self, uuid: db::Id)
        -> Result<usize, diesel::result::Error> {
            
        diesel::delete(persons.find(uuid)).execute(&self.connection)
    }
}

