#![feature(plugin)]
#![plugin(rocket_codegen)]

#[macro_use]    extern crate diesel;
                extern crate rocket;
                extern crate rocket_contrib;
                extern crate serde_json;
#[macro_use]    extern crate serde_derive;
#[macro_use]    extern crate diesel_codegen;

pub mod db;
pub mod data;

use std::path::{Path, PathBuf};
use std::sync::Mutex;
use std::collections::HashMap;
use std::result::Result;
use std::vec::Vec;

use rocket_contrib::{JSON, Template};
use rocket::response::{NamedFile, Redirect};
use rocket::State;

use data::ServerData;

type ManagedData = Mutex<ServerData>;

#[get("/")]
fn index() -> Redirect {
    Redirect::to("/index.html")
}

#[get("/<file..>")]
fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("resources/").join(file)).ok()
}

#[post("/create", format = "application/json", data = "<entry>")]
fn create(entry: JSON<db::PersonInfos>, data: State<ManagedData>)
    -> Result<(), diesel::result::Error> {
    
    let ref mut server_data = data.lock()
        .expect("Cannot take the lock on the data.");

    server_data.create(entry.into_inner()).map(|_|{})
}

#[post("/read")]
fn read(data: State<ManagedData>)
    -> Result<JSON<Vec<db::PersonEntry>>, diesel::result::Error> {
    
    let ref mut server_data = data.lock()
        .expect("Cannot take the lock on the data.");

    server_data.read().map(|payload| JSON(payload))
}

#[post("/update", format = "application/json", data = "<entry>")]
fn update(entry: JSON<db::PersonUpdate>, data: State<ManagedData>)
    -> Result<(), diesel::result::Error> {

    let ref mut server_data = data.lock()
        .expect("Cannot take the lock on the data.");

    server_data.update(entry.into_inner()).map(|_|{})
}

#[post("/delete", format = "application/json", data = "<id>")]
fn delete(id: JSON<db::Id>, data: State<ManagedData>)
    -> Result<(), diesel::result::Error> {

    let ref mut server_data = data.lock()
        .expect("Cannot take the lock on the data.");

    server_data.delete(id.into_inner()).map(|_|{})
}

#[error(404)]
fn not_found(req: &rocket::Request) -> Template {
    let mut map = HashMap::new();
    map.insert("path", req.uri().as_str());
    Template::render("error/404", &map)
}

fn main() {
    rocket::ignite()
        .mount("/", routes![
            index,
            files,
            create,
            read,
            update,
            delete
        ])
        .manage(Mutex::new(ServerData::new()))
        .catch(errors![not_found])
        .launch();
}
